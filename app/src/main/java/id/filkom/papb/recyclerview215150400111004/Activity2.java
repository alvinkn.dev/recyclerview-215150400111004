package id.filkom.papb.recyclerview215150400111004;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {
    TextView tvNim, tvName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        tvNim = findViewById(R.id.tvNim2);
        tvName = findViewById(R.id.tvNama2);
        String nim = getIntent().getStringExtra("nim");
        String nama = getIntent().getStringExtra("nama");
        tvNim.setText(nim);
        tvName.setText(nama);
    }
}